# Webinar 493 Creating APIs with NetTalk

Webinar 493

John Hickey will be presenting his journey into APIs using NetTalk! He&#39;ll talk about the need to allow outside developers to have access client&#39;s data, such as customer, inventory, and invoices, and the path he took to provide this functionality!

This repository contains the executable test app in the Output folder, along with the source app, project, and solution files.

The app is in Clarion 11, and requires the following templates:

CapeSoft:
	NetTalk
	StringTheory
	jfiles
	xfiles
	
ClarionLive Ultimate:
	Ultimate Class (optional)
	Ultimate Version (optional)
	Ultimate Debug
	UltimateNotify

Resources:

Apigee resources

https://apigee.com/api-management/#/resources

	Recommended:	API For Dummies
					Web API Design: The Missing Link
					

ngrok

https://ngrok.com
